import React, {
	useContext,
	useEffect,
	useState
} from 'react';
import ReactGA from "react-ga4";
import config from '../../app.config.json';

import 'tippy.js/dist/tippy.css';

import Header from '../Header';
import Footer from '../Footer';

import {
	SubscriptionDispatcher,
	Web3Context
} from '../../dispatchers';

import MintPage from '../MintPage';

export default function App() {
	
	const [ subscriptionContractAddress,  setSubscriptionContractAddress  ] = useState('');
	const {
		currentChain,
		userAddress
	} = useContext(Web3Context);
	
	useEffect(() => {
		// track GA
		ReactGA.initialize(config.GAid);
	}, []);
	
	useEffect(() => {
		
		getSubscriptionContractAddress();
		
		//eslint-disable-next-line
	}, [ currentChain, userAddress ]);
	
	const getSubscriptionContractAddress = () => {
		if ( !currentChain ) { return; }
		const foundChainConfig = config.CHAIN_SPECIFIC_DATA.find((item) => { return item.chainId === currentChain.chainId });
		if ( !foundChainConfig ) { return; }
		if ( !foundChainConfig.usersCollectionsContract ) { return; }
		setSubscriptionContractAddress(foundChainConfig.usersCollectionsContract);
	}
	
	
	return (
		<SubscriptionDispatcher
			TX_names={{ singular: 'mint', plural: 'mints' }}
			serviceContract={ subscriptionContractAddress }
		>	
			<Header />
			<MintPage />
			<Footer />
		</SubscriptionDispatcher>
	)

}