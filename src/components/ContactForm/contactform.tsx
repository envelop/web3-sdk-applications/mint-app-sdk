import React, { useState, Dispatch, SetStateAction } from 'react';
import emailjs from '@emailjs/browser';

import icon_loading from '../../static/pics/loading.svg';

import config from '../../app.config.json';

type ContactFormProps = {
	feedbackSent: boolean,
    setFeedbackSent: Dispatch<SetStateAction<boolean>>
}

export default function ContactForm(props: ContactFormProps) {

    const [sendingMsg, setSendingMsg] = useState<boolean>(false);
    const [nameVal, setNameVal] = useState<string>('');
    const [emailVal, setEmailVal] = useState<string>('');
    const [messageVal, setMessageVal] = useState<string>('');

    const form = React.useRef(null);

    const sendEmail = (e: any) => {
        e.preventDefault();

        setSendingMsg(true);

        emailjs.sendForm(config.EmailJS.SERVICE_ID, config.EmailJS.TEMPLATE_ID, form.current as any, { publicKey: config.EmailJS.PUBLIC_KEY }).then(() => {
            console.log('Message sent!');
            props.setFeedbackSent(true);
        },(error) => {
            console.log('Message sending failed', error.text);
        });
    };

    return (
        <>
            { !props.feedbackSent ? (
            <form ref={form} onSubmit={sendEmail}>
                <div className="row m-4">
                    <div className="col-12 col-md-6">
                        <div className="input-group">
                            <label className="input-label" htmlFor="user_name">Name</label>
                            <input className="input-control" placeholder="Name" type="text" name="user_name" value={nameVal} onChange={e => setNameVal(e.target.value)} />
                        </div>
                    </div>
                    <div className="col-12 col-md-6">
                        <div className="input-group">
                            <label className="input-label" htmlFor="user_email">Email</label>
                            <input className="input-control" placeholder="Email" type="email" name="user_email" value={emailVal} onChange={e => setEmailVal(e.target.value)} />
                        </div>
                    </div>
                    <div className="col-12">
                        <div className="input-group">
                            <label className="input-label" htmlFor="message">Message</label>
                            <textarea className="input-control" name="message" value={messageVal} onChange={e => setMessageVal(e.target.value)} />
                        </div>
                    </div>
                    <div className="col-12">
                        <input className="btn w-100" type="submit" value={ sendingMsg ? ("Sending") : ("Send") } disabled={ !nameVal || !emailVal || !messageVal || sendingMsg } />
                        { sendingMsg ? (
                                <span className="i-coin ml-2">
                                    <img src={ icon_loading } alt="" />
                                </span>
                            ) : null
                        }
                    </div>
                </div>
            </form>
            ) : <p>Message has been sent. Thanks!</p> }
        </>
    );

}