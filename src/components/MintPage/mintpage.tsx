
import React, {
	useContext,
	useEffect,
	useState,
	useRef
} from "react";
import { CopyToClipboard }  from 'react-copy-to-clipboard';
import Dropzone from 'react-dropzone';
import { CookiesProvider, useCookies } from 'react-cookie';
import { Driver, driver } from "driver.js";
import "driver.js/dist/driver.css";
import {
	ChainType,
	Web3,
	_AssetType,
	decodeAssetTypeFromIndex,
	getChainId,
	getContractNameERC1155,
	getContractNameERC721,
} from "@envelop/envelop-client-core";

import TippyWrapper        from "../TippyWrapper";
import NumberInput         from "../NumberInput";
import CoinSelector        from "../CoinSelector";
import useGATracker        from "../GAEventTracker";
import ContactForm         from "../ContactForm";

import {
	AdvancedLoaderStageType,
	InfoModalContext,
	SubscriptionContext,
	SubscriptionRenderer,
	Web3Context,
	_AdvancedLoadingStatus,
	_ModalTypes
} from "../../dispatchers";

import default_token_preview from '@envelop/envelop-client-core/static/pics/_default_nft.svg';
import icon_i_del            from '../../static/pics/i-del.svg';
import icon_i_drop          from '../../static/pics/icons/i-wrap.svg';
import icon_i_copy           from '../../static/pics/icons/i-copy.svg';
import icon_i_arrow           from '../../static/pics/icons/i-arrow-curved.svg';
import icon_external         from '../../static/pics/icons/i-external.svg';
import icon_loader           from '../../static/pics/loader-orange.svg';
import icon_loading          from '../../static/pics/loading.svg';
import icon_success          from '../../dispatchers/InfoModalDispatcher/icons/success.png';
import icon_info          from '../../dispatchers/InfoModalDispatcher/icons/info.png';

import axios from "axios";

import config                from '../../app.config.json';

import {
	checkSubscriptionEnabled,
	getUserCollection,
	getTotalSupply721,
	getTotalSupply1155,
	updateTotalSupply1155,
	getContractName,
	fetchSwarmStamp,
	fetchAIData,
	getOracleNftMinterSign,
	mintToken,
	buySubscription,
	deployNewCollection
} from "../../models/mint";


export default function MintPage() {

	let   scrollable: any = {};
	const niftsyTokenIcon = 'https://envelop.is/assets/img/niftsy.svg';

	const driverObj = driver({
		showProgress: true,
		steps: [
			{ popover: { title: 'Mint NFT Quick Guide', description: 'Learn how to mint an NFT in this simple tour.' }},
			{ element: '.nft-upload', popover: { title: 'Step 1 - Image', description: 'Click here to choose your image from your computer.', side: "left", align: 'start' }},
			{ element: '.nft-name', popover: { title: 'Step 2 - Name', description: 'Input a Title for your NFT here.', side: "bottom", align: 'start' }},
			{ element: '.nft-desc', popover: { title: 'Step 3 - Description', description: 'Create a decent description for your NFT here.', side: "bottom", align: 'start' }},
			{ element: '.nft-mint-btn', popover: { title: 'Step 4 - Mint', description: 'Now Mint it! Confirm transaction in your wallet and wait for your newly created NFT contract address and ID.', side: "left", align: 'start' }},
			{ element: '.ai-generate', popover: { title: 'OR Step 1-2-3 in ONE', description: 'Let an AI to generate all of this - Image, Title and Description - for you!.', side: "bottom", align: 'start' }},
			{ popover: { title: 'Happy Minting!', description: 'Have fun with NFT minting and feel free to leave us a feedback!' }}
		]
	  });

	const {
		userAddress,
		currentChain,
		balanceNative,
		web3,
		getWeb3Force,
		switchChain,
	} = useContext(Web3Context);
	const {
		setModal,
		setError,
		unsetModal,
		setLoading,
		createAdvancedLoader,
		updateStepAdvancedLoader,
	} = useContext(InfoModalContext);
	const {
		subscriptionRemainings,
		subscriptionServiceExist,
	} = useContext(SubscriptionContext);

	const [ pageMode,                        setPageMode                        ] = useState<'simple' | 'advanced'>('simple');
	const [ showErrors,                      setShowErrors                      ] = useState<Array<string>>([]);
	const [ currentWidth,                   setCurrentWidth                    ] = useState(window.innerWidth);
	const [ nftminterContract721,                 setNftminterContract721            ] = useState<string>('');
	const [ nftminterContract1155,                 setNftminterContract1155           ] = useState<string>('');
	const [ implContract721,                          setImplContract721                 ] = useState<string>('');
	const [ implContract1155,                     setImplContract1155                ] = useState<string>('');
	const [ usersCollectionsContract,                 setUsersCollectionsContract        ] = useState<string | undefined>('');
	const [ inputOutAssetType,               setInputOutAssetType               ] = useState<_AssetType>(_AssetType.ERC721);
	const [ inputBatch,                     setInputBatch                      ] = useState(1);
	const [ inputCopies,                     setInputCopies                     ] = useState(2);
	const [ metaHostingType,                     setMetaHostingType                 ] = useState(2);
	const [ metaPinataApi,                     setMetaPinataApi                   ] = useState('');
	const [ fileReader,                      setFileReader                      ] = useState<Array<string>>([]);
	const [ nftImagePreview,                   setNftImagePreview                 ] = useState<string | ArrayBuffer>('');
	const [ nftImageMimeType,                      setNftImageMimeType                ] = useState('');
	const [ inputNftImageUrl,                      setInputNftImageUrl                ] = useState('');
	const [ withAIPrompt,                      setWithAIPrompt                    ] = useState('');
	const [ inputNftInfoName,                      setInputNftInfoName                ] = useState('');
	const [ inputNftInfoDesc,                      setInputNftInfoDesc                ] = useState('');
	const [ descriptionRows,                  setDescriptionRows                 ] = useState<number>(5);
	const [ nftPropertiesData,                      setNftPropertiesData               ] = useState<Array<{ type: string, name: string }>>([]);
	const [ nftPropertiesDataTmp,                      setNftPropertiesDataTmp            ] = useState<Array<{ type: string, name: string }>>([]);
	const [ uploadedNftImageUrl,                      setUploadedNftImageUrl             ] = useState('');
	const [ uploadedNftJsonUrl,                      setUploadedNftJsonUrl              ] = useState('');
	const [ mintedNftTxId,                      setMintedNftTxId                   ] = useState('');
	const [ mintedNftContract,                      setMintedNftContract               ] = useState<string | undefined>('0x1');
	const [ mintedNftTokenId,                      setMintedNftTokenId                ] = useState<string | undefined>('0,1');	
	const [ feedbackSent,                       setFeedbackSent                    ] = useState<boolean>(false);
	const [ feedbackModalOpened,                   setFeedbackModalOpened             ] = useState<boolean>(false);
	const [ modeModalOpened,                   setModeModalOpened                 ] = useState<boolean>(false);
	const [ imageModalOpened,                   setImageModalOpened                ] = useState<boolean>(false);
	const [ withAIModalOpened,                   setWithAIModalOpened               ] = useState<boolean>(false);
	const [ withAIImage,                             setWithAIImage                     ] = useState<boolean>(true);
	const [ withAIModel,                     setWithAIModel                     ] = useState(2);
	const [ withAITitle,                             setWithAITitle                     ] = useState<boolean>(false);
	const [ sendingAIPrompt,                   setSendingAIPrompt                 ] = useState<boolean>(false);
	const [ usingAIImage,                             setUsingAIImage                    ] = useState<boolean>(false);
	const [ copiedHint,                          setCopiedHint                      ] = useState<number>(-1);
	const [ nftPropertiesOpened,                   setNftPropertiesOpened             ] = useState<boolean>(false);
	const [ advancedOptionsOpened,                   setAdvancedOptionsOpened           ] = useState<boolean>(false);
	const [ savingMetadataOpened,                   setSavingMetadataOpened            ] = useState<boolean>(false);
	const [ mintFactoryPopupOpened,                   setMintFactoryPopupOpened          ] = useState<boolean>(false);
	const [ deployingPopupOpened,                   setDeployingPopupOpened            ] = useState<boolean>(false);
	const [ nftMintedPopupOpened,                   setNftMintedPopupOpened            ] = useState<boolean>(false);
	const [ usersCollectionsChecked,                   setUsersCollectionsChecked         ] = useState<boolean>(false);
	const [ mintFactoryContractCreated,                   setMintFactoryContractCreated      ] = useState<boolean>(false);
	const [ mintFactoryContractName,                   setMintFactoryContractName         ] = useState('');
	const [ mintFactoryContractSymbol,                   setMintFactoryContractSymbol       ] = useState('');
	const [ mintFactoryContractAddress,                   setMintFactoryContractAddress      ] = useState('');
	const [ mintFactoryContractList721,                      setMintFactoryContractList721      ] = useState<Array<{type: _AssetType, name: string, address: string }>>([{ type: _AssetType.ERC721, name: 'Default Contract', address: '' }]);
	const [ mintFactoryContract721Index,                  setMintFactoryContract721Index     ] = useState<number>(0);
	const [ mintFactoryContractList1155,                      setMintFactoryContractList1155     ] = useState<Array<{type: _AssetType, name: string, address: string }>>([{ type: _AssetType.ERC1155, name: 'Default Contract', address: '' }]);
	const [ mintFactoryContract1155Index,                  setMintFactoryContract1155Index    ] = useState<number>(0);
	const [ isSubscriptionEnabled,                   setIsSubscriptionEnabled           ] = useState<boolean>(false);

	// use cookies
	const [cookies, setCookie          ] = useCookies(['tourViewed']);
	
	// track GA event
	const gaEventTracker = useGATracker.useGAEventTracker('Mint');

	useEffect(() => {
		const getMintParams = async () => {
			if ( !currentChain ) { return; }

			const foundChain = config.CHAIN_SPECIFIC_DATA.find((item) => { return item.chainId === currentChain.chainId });
			if ( !foundChain ) {
				setError('Unsupported chain');
				return;
			}

			const _nftminterContract721 = foundChain.nftMinterContract721;
			if ( _nftminterContract721 ) {
				setNftminterContract721(_nftminterContract721);
				setMintFactoryContractList721([{ type: _AssetType.ERC721, name: 'Default Contract', address: _nftminterContract721 }]);

			}

			const _nftminterContract1155 = foundChain.nftMinterContract1155;
			if ( _nftminterContract1155 ) {
				setNftminterContract1155(_nftminterContract1155);
				setMintFactoryContractList1155([{ type: _AssetType.ERC1155, name: 'Default Contract', address: _nftminterContract1155 }]);
			}

			const _implContract721 = foundChain.impl721Contract;
			if ( _implContract721 ) {
				setImplContract721(_implContract721);
			}

			const _implContract1155 = foundChain.impl1155Contract;
			if ( _implContract1155 ) {
				setImplContract1155(_implContract1155);
			}

			const _usersCollectionsContract = foundChain.usersCollectionsContract;
			setUsersCollectionsContract(_usersCollectionsContract);
			if ( _usersCollectionsContract ) {
				// Subscription Enabled checking for Sepolia
				if (currentChain.chainId === 11155111) {
					let isEnabled = await checkSubscriptionEnabled(currentChain.chainId, _usersCollectionsContract);
					console.log("checkSubscriptionEnabled", isEnabled);
					setIsSubscriptionEnabled(isEnabled);
				}
			}

			console.log('nftMinterContract721', _nftminterContract721);
			console.log('nftMinterContract1155', _nftminterContract1155);
			console.log('usersCollectionsContract', _usersCollectionsContract);

			if( pageMode === 'advanced' && _usersCollectionsContract) {
				getUserCollectionData(_nftminterContract721, _nftminterContract1155, _usersCollectionsContract, _implContract721, _implContract1155);
			}

		}

		getMintParams();
		setMetaPinataApi(config.PINATA_AUTH);
	}, [ currentChain, userAddress ]);

	useEffect(() => {
		if ( !subscriptionServiceExist ) { return; }
		if ( !subscriptionRemainings ) { return; }
		// if ( subscriptionRemainings.countsLeft.gt(0) ) { setFormLocked(false); }

		// const now = new BigNumber(new Date().getTime()).dividedToIntegerBy(1000);
		// if ( subscriptionRemainings.validUntil.gt(now) ) {
		// 	setFormLocked(false);
		// }
		console.log("subscriptionRemainings",subscriptionRemainings.countsLeft);
		console.log("subscriptionRemainings",subscriptionRemainings.validUntil);

	}, [ subscriptionRemainings ]);

	useEffect(() => {
		if(feedbackSent) {
			// track GA event
			console.log('Form sent event');
			gaEventTracker('feedback_form_submit','Feedback Submit');
		}
	}, [feedbackSent]);

	useEffect(() => {
		// track GA pageview
		useGATracker.useGAPageTracker(window.location.pathname + window.location.search, "Mint dApp view");
		// start a tour
		if(!cookies.tourViewed) {
			driverObj.drive();			
			setCookie('tourViewed', 1, { path: '/mintnew' });
		}		
	}, []);

	useEffect(() => {
		window.addEventListener("resize", updateDimensions);
		return () => window.removeEventListener("resize", updateDimensions);
	}, []);

	const updateDimensions = () => {
		setCurrentWidth(window.innerWidth);
	}

	const getModeSelector = () => {
		return (
			<div className="col-12 col-md-auto mb-3">
				<div className="wf-settings__switcher">
					<div className="label d-none d-sm-block">Mint mode</div>
					<div className="label d-sm-none">Mode</div>
					<div className="switcher">
						<input
							className="toggle toggle-left"
							type="radio"
							id="simple-on"
							checked={ pageMode === 'simple' }
							onChange={() => {
								setPageMode('simple');
								setInputOutAssetType(_AssetType.ERC721);
								setAdvancedOptionsOpened(false);
							}}
						/>
						<label
							className="switcher__btn"
							htmlFor="simple-on"
						>
							Simple
						</label>
						<input
							className="toggle toggle-right"
							id="advanced-on"
							value="true"
							type="radio"
							checked={ pageMode === 'advanced' }
							onChange={() => {
								setPageMode('advanced');
								setAdvancedOptionsOpened(true);
								if (nftminterContract721 && nftminterContract1155 && usersCollectionsContract) {
									getUserCollectionData(nftminterContract721, nftminterContract1155, usersCollectionsContract, implContract721, implContract1155);
								}
							}}
						/>
						<label
							className="switcher__btn"
							htmlFor="advanced-on"
						>Advanced</label>
					</div>
					<div
						className="select-coin text-green ml-2 d-sm-block"
						onClick={() => { setModeModalOpened(true) }}
						><span className="i-tip "></span><span className="d-none d-sm-inline-block">What is Advanced Mode?</span></div>
				</div>
			</div>
		)
	}

	const getUserCollectionData = async (_nftminterContract721: string, _nftminterContract1155: string, _usersCollectionsContract: string, _implContract721: string, _implContract1155: string) => {
		if ( !currentChain ) { return; }

		if ( userAddress ) {
			const _userAddress = userAddress;
			setUsersCollectionsChecked(false);

			await getUserCollection(currentChain.chainId, _usersCollectionsContract,_userAddress)
				.then((data: any) => {
					if (data instanceof Array) {

						// filter results by asset type
						const data_721_tmp = data.filter(item => {
							return item.assetType === _AssetType.ERC721;
						});
						const data_1155_tmp = data.filter(item => {
							return item.assetType === _AssetType.ERC1155;
						});

						// convert results to objects
						let data_721_new = data_721_tmp.map( async (item, idx) => {
							const name = await getContractName(currentChain.chainId, item.contractAddress, _implContract721);
							return {
								type: decodeAssetTypeFromIndex(item.assetType),
								name: (name) ? name : `CustomContract${idx+1}`,
								address: item.contractAddress as string,
							}
						});
						let data_1155_new = data_1155_tmp.map( async (item, idx) => {
							const name = await getContractName(currentChain.chainId, item.contractAddress, _implContract1155);
							return {
								type: decodeAssetTypeFromIndex(item.assetType),
								name: (name) ? name : `CustomContract${idx+1}`,
								address: item.contractAddress as string,
							}
						});

						// saving final objects
						Promise.all(data_721_new).then(data => {
							const data_721 = [{ type: _AssetType.ERC721, name: 'Default Contract', address: _nftminterContract721 },...data];
							setMintFactoryContractList721(data_721);
						});
						Promise.all(data_1155_new).then(data => {
							const data_1155 = [{ type: _AssetType.ERC1155, name: 'Default Contract', address: _nftminterContract1155 },...data];
							setMintFactoryContractList1155(data_1155);
						});
						setUsersCollectionsChecked(true);
					}
				})
				.catch((error) => {
					console.log('Cannot get user collection', error);
					setMintFactoryContractList721([{ type: _AssetType.ERC721, name: 'Default Contract', address: _nftminterContract721 }]);
					setMintFactoryContractList1155([{ type: _AssetType.ERC1155, name: 'Default Contract', address: _nftminterContract1155 }]);
					setUsersCollectionsChecked(true);
				});
		}
	}

	const getConnectBtn = (label: string) => {
		return (
			<div>
				<button
					className="btn btn-connect"
					onClick={async (e) => {
						try { await getWeb3Force(); } catch(e: any) { console.log('Cannot connect', e); }
						gaEventTracker('mint_connect_btn','Connect wallet');
					}}
				>
					Connect Wallet
				</button>
				<br /> { label }
			</div>
		)
	}

	const showLoader = () => {
		if ( userAddress && usersCollectionsContract && !usersCollectionsChecked) {
			return (
				<span className="i-coin">
					<img src={ icon_loading } alt="" />
				</span>
			)
		}
	}

	const getMintFactoryBlock = () => {
		if ( pageMode === 'simple' ) { return null; }

		return (
					<div className="col-12">
						<p>Available { currentChain?.EIPPrefix || 'ERC' }-{(inputOutAssetType === _AssetType.ERC721) ? '721' : '1155'} contracts list:
							<TippyWrapper
								msg="Use default Mint contract or create your personal Mint contracts"
							></TippyWrapper>
						</p>
						<div className="my-contracts">
						{ (inputOutAssetType === _AssetType.ERC721) ?
							mintFactoryContractList721.map((contract: any, idx: any) => {
								return (
									<div className="mb-3" key={idx}>
										<label className="checkbox">
											<input
												type="radio"
												name="my-contracts"
												value={idx}
												checked={ mintFactoryContract721Index === idx }
												onChange={() => { setMintFactoryContract721Index(idx) }}
											/>
											<span className="check"> </span>
											<span className={ `check-text ${ (mintFactoryContract721Index === idx) ? 'check-text-bold' : '' }`}>{ (mintFactoryContract721Index === idx) ? ( <b>{ contract.name } - { shortenHash(contract.address) }</b> ) : contract.name + ' - ' + shortenHash(contract.address) }
											</span>
											<CopyToClipboard
												text={ contract.address }
												onCopy={() => {
													setCopiedHint(idx);
													setTimeout(() => {
														setCopiedHint(-1);
													}, 3*1000);
												}}
											>
												<button className="btn-copy">
													&nbsp;<img src={ icon_i_copy } alt="" />
													<span className="btn-action-info" style={{ display: copiedHint === idx ? 'block' : 'none' }}>Copied</span>
												</button>
											</CopyToClipboard>
										</label>
									</div>
								)
							})
							: mintFactoryContractList1155.map( (contract: any, idx: any) => {
								return (
									<div className="mb-3" key={idx}>
										<label className="checkbox">
											<input
												type="radio"
												name="my-contracts"
												value={idx}
												checked={ mintFactoryContract1155Index === idx }
												onChange={() => { setMintFactoryContract1155Index(idx) }}
											/>
											<span className="check"> </span>
											<span className={ `check-text ${ (mintFactoryContract1155Index === idx) ? 'check-text-bold' : '' }`}>{ (mintFactoryContract1155Index === idx) ? ( <b>{ contract.name } - { shortenHash(contract.address) }</b> ) : contract.name + ' - ' + shortenHash(contract.address) }
											</span>
											<CopyToClipboard
												text={ contract.address }
												onCopy={() => {
													setCopiedHint(idx);
													setTimeout(() => {
														setCopiedHint(-1);
													}, 3*1000);
												}}
											>
												<button className="btn-copy">
													&nbsp;<img src={ icon_i_copy } alt="" />
													<span className="btn-action-info" style={{ display: copiedHint === idx ? 'block' : 'none' }}>Copied</span>
												</button>
											</CopyToClipboard>
										</label>
									</div>
								)
							})
						}
						{ showLoader() }
						</div>
						<div className="d-inline-block mr-2 my-3">
							{ userAddress === undefined ? getConnectBtn("to view your personal Mint contracts") : ( usersCollectionsContract ? (
								<button
									className="btn btn-outline"
									onClick={() => {
										if(mintFactoryContractCreated) {
											resetMintFactoryContractCreated();
										}
										setMintFactoryPopupOpened(true);
									}}
								>Create personal { currentChain?.EIPPrefix || 'ERC' }-{(inputOutAssetType === _AssetType.ERC721) ? '721' : '1155'} Mint contract</button>
							) : <i>No custom contracts support on this chain</i> ) }
						</div>
					</div>
		)
	}

	const showMintFactoryPopup = () => {
		return (
			<div
				className="modal"
				onClick={(e) => {
					e.stopPropagation();

					const el = (e.target as HTMLTextAreaElement);
					const parentEl = el.parentElement;

					if ( !el       ) { return; }
					if ( !parentEl ) { return; }

					if (
						parentEl.className.includes('input-control') ||
						parentEl.className.includes('select-custom') ||
						parentEl.className.includes('option-token')
					) { return; }

					if ((e.target as HTMLTextAreaElement).className === 'modal__inner') {
						setMintFactoryPopupOpened(false);
						resetMintFactoryContractCreated();
					}

				}}
			>
			<div
					className="modal__inner"
					onMouseDown={(e) => {
						e.stopPropagation();
						if ((e.target as HTMLTextAreaElement).className === 'modal__inner' || (e.target as HTMLTextAreaElement).className === 'container') {
							setMintFactoryPopupOpened(false);
							resetMintFactoryContractCreated();
						}
					}}
				>
			<div className="modal__bg"></div>
			<div className="container">
			<div className="modal__content">
				<div
					className="modal__close"
					onClick={() => {
						setMintFactoryPopupOpened(false);
						resetMintFactoryContractCreated();
					}}
				>
					<svg width="37" height="37" viewBox="0 0 37 37" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path fillRule="evenodd" clipRule="evenodd" d="M35.9062 36.3802L0.69954 1.17351L1.25342 0.619629L36.4601 35.8263L35.9062 36.3802Z" fill="white"></path>
						<path fillRule="evenodd" clipRule="evenodd" d="M0.699257 36.3802L35.9059 1.17351L35.3521 0.619629L0.145379 35.8263L0.699257 36.3802Z" fill="white"></path>
					</svg>
				</div>
				<div className="c-add">
					<div className="c-add__text">
						<div className="h2">Create your personal Mint contract</div>
					</div>
					{
						!mintFactoryContractCreated ? (
							<div className="c-add__form">

							<div className="row mb-3">
								<div className="col-12"><span className="input-label">Choose a name and a symbol for your personal Token Collection</span></div>
							</div>
							<div className="row mb-6">
								<div className="col-12 col-lg-6">
									<textarea
										className="input-control"
										placeholder="Token Collection Name"
										rows={1}
										value={ mintFactoryContractName }
										onChange={(e) => { setMintFactoryContractName(e.target.value.charAt(0).toUpperCase() + e.target.value.slice(1)) }}
									></textarea>
								</div>
								<div className="col-12 col-lg-6">
									<textarea
										className="input-control"
										placeholder="Token Collection Symbol"
										rows={1}
										maxLength={16}
										value={ mintFactoryContractSymbol }
										onChange={(e) => { setMintFactoryContractSymbol(e.target.value.toUpperCase()) }}
									></textarea>
								</div>
							</div>
							<div className="row">
								<div className="col-sm-3 mb-2 mb-sm-0">
								</div>
								<div className="col-sm-6">
									<div className="c-wrap mb-0">
										<button
											className="btn btn-grad w-100"
											disabled={ isMintFactorySubmitDisabled() }
											onClick={async () => {
												if ( !currentChain ) { return; }
												let _web3 = web3;
												let _userAddress = userAddress;
												let _currentChain = currentChain;

												if ( !web3 || !userAddress || await getChainId(web3) !== _currentChain.chainId ) {
													setLoading('Waiting for wallet');

													try {
														const web3Params = await getWeb3Force(_currentChain.chainId);

														_web3 = web3Params.web3;
														_userAddress = web3Params.userAddress;
														_currentChain = web3Params.chain;
														unsetModal();
													} catch(e: any) {
														setModal({
															type: _ModalTypes.error,
															title: 'Error with wallet',
															details: [
																e.message || e
															]
														});
													}
												}

												if ( !_web3 || !_userAddress || !_currentChain ) { return; }
												deploySubmit(_currentChain, _web3, _userAddress);
											}}
										>Create Mint Contract</button>
									</div>
								</div>
							</div>
							</div>
						) : (
							<div className="c-add__form">
								<div className="row mb-3">
									<div className="col-12"><span className="input-label">Your personal Mint Contract has been created!</span></div>
								</div>
								<div className="row mb-3">
									<div className="col-12"><b>{ mintFactoryContractName }</b> at <a
										className="ex-link"
										target="_blank"
										rel="noopener noreferrer"
										href={ `${currentChain?.explorerBaseUrl || ''}address/${mintFactoryContractAddress}` }
										>{ mintFactoryContractAddress } <img className="i-ex" src={ icon_external } alt="" /></a>
									</div>
								</div>
							</div>
						)
					}
				</div>
			</div>
			</div>
			</div>
			</div>
		)
	}

	const deploySubmit = async (_currentChain: ChainType, _web3: Web3, _userAddress: string) => {

		setMintFactoryPopupOpened(false);
		setDeployingPopupOpened(true);
		setSavingMetadataOpened(true);

		let _implContract = ( inputOutAssetType === _AssetType.ERC721 ) ? implContract721 : implContract1155 ;
		let _registryContract = (usersCollectionsContract) ? usersCollectionsContract : '';

		// deploy collection for user
		const deployArgs = {
			chainId: _currentChain.chainId,
			_name: mintFactoryContractName,
			_symbol: mintFactoryContractSymbol,
			_baseurl: '',
		};
		let txResp;
		try {
			console.log('_implContract', _implContract);
			console.log('_registryContract', _registryContract);
			console.log('deployArgs', deployArgs);
			txResp = await deployNewCollection(
				_web3,
				_implContract,
				_registryContract,
				deployArgs,
			);
		} catch(e: any) {
			console.log('Error:', e.message);
			setDeployingPopupOpened(false);
			setSavingMetadataOpened(false);
			setModal({
				type: _ModalTypes.error,
				title: `Cannot deploy new collection`,
				details: [
					`Args: ${JSON.stringify(deployArgs)}`,
					'',
					e.message || e,
				]
			});
			return;
		}
		if ('transactionHash' in txResp) {
			let deployedContract = '';
			try { deployedContract = txResp.events[0].address; } catch(ignored) {}

			console.log(txResp);
			console.log("Deployed contract " + deployedContract + " with txid " + txResp['transactionHash']);
			setDeployingPopupOpened(false);
			setSavingMetadataOpened(false);

			setMintFactoryContractAddress(deployedContract);
			setMintFactoryContractCreated(true);
			resetMintFactoryContractCreated();

			if ( inputOutAssetType === _AssetType.ERC721 ) {
				setMintFactoryContractList721([...mintFactoryContractList721, {
					type: inputOutAssetType,
					name: mintFactoryContractName,
					address: deployedContract,
				}]);
				setMintFactoryContract721Index(mintFactoryContractList721.length);
			}
			if ( inputOutAssetType === _AssetType.ERC1155 ) {
				setMintFactoryContractList1155([...mintFactoryContractList1155, {
					type: inputOutAssetType,
					name: mintFactoryContractName,
					address: deployedContract,
				}]);
				setMintFactoryContract1155Index(mintFactoryContractList1155.length);
			}

			setModal({
				type: _ModalTypes.success,
				title: `Custom Contract has been deployed!`,
				copyables: [
					{ title: 'Contract address of new collection', content: deployedContract },
				],
				buttons: [{
					text: 'Ok',
					clickFunc: () => {
						unsetModal();
					},
				}],
				links: [{
					text: `View on ${_currentChain.explorerName}`,
					url: `${_currentChain.explorerBaseUrl}/tx/${txResp.transactionHash}`
				}],
			});
		}
		else {
			console.log('No TxID in response');
			setModal({
				type: _ModalTypes.error,
				title: `Cannot deploy Contract`,
				details: [
					`Please try again later.`,
					'',
				]
			});
			return;
		}
	}

	const isMintFactorySubmitDisabled = () => {
		if ( !mintFactoryContractName || !mintFactoryContractSymbol) {
			return true;
		}
		return false
	}

	const resetMintFactoryContractCreated = () => {
		if(mintFactoryContractCreated) {
			setMintFactoryContractName('');
			setMintFactoryContractSymbol('');
			setMintFactoryContractCreated(false);
		}
	}

	const showNftMintedPopup = () => {

		if ( !currentChain ) { return; }

		// check supported chains for launchpad link
		const launchpadLinks = config.navigationLinks.find((item) => { return item.url === "/launchpad_admin" });
		const launchpadChains = launchpadLinks?.showInChains;
		const showSellBtn = (launchpadChains?.includes(currentChain.chainId)) ? true : false;

		return (
			<div
				className="modal"
				onClick={(e) => {
					e.stopPropagation();

					const el = (e.target as HTMLTextAreaElement);
					const parentEl = el.parentElement;

					if ( !el       ) { return; }
					if ( !parentEl ) { return; }

					if (
						parentEl.className.includes('input-control') ||
						parentEl.className.includes('select-custom') ||
						parentEl.className.includes('option-token')
					) { return; }

					if ((e.target as HTMLTextAreaElement).className === 'modal__inner') {
						setNftMintedPopupOpened(false);
					}

				}}
			>
			<div
					className="modal__inner"
					onMouseDown={(e) => {
						e.stopPropagation();
						if ((e.target as HTMLTextAreaElement).className === 'modal__inner' || (e.target as HTMLTextAreaElement).className === 'container') {
							setNftMintedPopupOpened(false);
						}
					}}
				>
			<div className="modal__bg"></div>
			<div className="container">
			<div className="modal__content">
				<div
					className="modal__close"
					onClick={() => {
						setNftMintedPopupOpened(false);
					}}
				>
					<svg width="37" height="37" viewBox="0 0 37 37" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path fillRule="evenodd" clipRule="evenodd" d="M35.9062 36.3802L0.69954 1.17351L1.25342 0.619629L36.4601 35.8263L35.9062 36.3802Z" fill="white"></path>
						<path fillRule="evenodd" clipRule="evenodd" d="M0.699257 36.3802L35.9059 1.17351L35.3521 0.619629L0.145379 35.8263L0.699257 36.3802Z" fill="white"></path>
					</svg>
				</div>
				<div className="c-info">
					<div className="c-info__img"><img src={ icon_success } alt="" /></div>
					<div className="c-info__text">
						<div className="h2">NFT has been minted!</div>
						<div className="row row-sm mb-4 mt-4">
							<div className="col-12">View it on <a
								className="ex-link"
								target="_blank"
								rel="noopener noreferrer"
								href={ `${currentChain?.explorerBaseUrl || ''}/tx/${mintedNftTxId}` }
								>{currentChain?.explorerName} <img className="i-ex" src={ icon_external } alt="" /></a>
							</div>
						</div>
						<div className="row mb-4">
							<div className="col-12">
								<label className="input-label">Contract address</label>
							</div>
							<div className="col-sm-10 pr-sm-0">
								<input className="input-control control-gray" type="text" readOnly value={ mintedNftContract !== undefined ? mintedNftContract : '' } />
							</div>
							<div className="col-sm-2 mt-3 mt-sm-0">{
									mintedNftContract !== undefined ? (
									<CopyToClipboard
										text={ mintedNftContract }
										onCopy={() => {
											setCopiedHint(0);
											setTimeout(() => {
												setCopiedHint(-1);
											}, 3*1000);
										}}
									>
										<button className="btn btn-gray w-100">
											&nbsp;<img src={ icon_i_copy } alt="" />
											<span className="btn-action-info" style={{ display: copiedHint === 0 ? 'block' : 'none' }}>Copied</span>
										</button>
									</CopyToClipboard>
									) : null }
							</div>
						</div>
						<div className="row mb-4">
							<div className="col-12">
								<label className="input-label">Token ID(s)</label>
							</div>
							<div className="col-sm-10 pr-sm-0">
								<input className="input-control control-gray" type="text" readOnly value={ mintedNftTokenId !== undefined ? mintedNftTokenId : '' } />
							</div>
							<div className="col-sm-2 mt-3 mt-sm-0">{
									mintedNftTokenId !== undefined ? (
									<CopyToClipboard
										text={ mintedNftTokenId }
										onCopy={() => {
											setCopiedHint(1);
											setTimeout(() => {
												setCopiedHint(-1);
											}, 3*1000);
										}}
									>
										<button className="btn btn-gray w-100">
											&nbsp;<img src={ icon_i_copy } alt="" />
											<span className="btn-action-info" style={{ display: copiedHint === 1 ? 'block' : 'none' }}>Copied</span>
										</button>
									</CopyToClipboard>
									) : null }
							</div>
						</div>
						<div className="row row-sm mt-6">
							<div className="col-12">
								<div style={{
									width: "max-content",
									maxWidth: "100%",
									minHeight: "95px",
									paddingTop: "10px",
									paddingRight: "72px",
									backgroundImage: `url(${icon_i_arrow})`,
									backgroundPosition: 'right',
									backgroundRepeat: 'no-repeat'
								}}>Ok, it's minted. What's next? Try to wrap it into <strong>Smart NFT</strong></div>
							</div>
						</div>
						{ showSellBtn ? (
							<div className="row row-sm mt-2" >
								<div className="col-12 col-sm-4 mb-3" style={{ order: currentWidth < 415 ? "3" : "1"}}>
									<button
										className="btn w-100 btn-outline"
										onClick={() => {
											window.location.href = '/dashboard';
										}}>To dashboard</button>
								</div>
								<div className="col-12 col-sm-4 mb-3" style={{ order: currentWidth < 415 ? "2" : "2"}}>
									<button
										className="btn w-100 btn-outline"
										onClick={() => {
											window.location.href = '/launchpad_admin';
										}}>Sell</button>
								</div>
								<div className="col-12 col-sm-4 mb-3" style={{ order: currentWidth < 415 ? "1" : "3"}}>
									<button
										className="btn w-100 btn"
										onClick={() => {
											window.location.href = '/wrap';
										}}>Wrap</button>
								</div>
							</div>
							) : (
							<div className="row row-sm mt-2" >
								<div className="col-12 col-sm-6 mb-3" style={{ order: currentWidth < 415 ? "2" : "1"}}>
									<button
										className="btn w-100 btn-outline"
										onClick={() => {
											window.location.href = '/dashboard';
										}}>To dashboard</button>
								</div>
								<div className="col-12 col-sm-6 mb-3" style={{ order: currentWidth < 415 ? "1" : "2"}}>
									<button
										className="btn w-100 btn"
										onClick={() => {
											window.location.href = '/wrap';
										}}>Wrap</button>
								</div>
							</div>								
							) }	
					</div>
				</div>
			</div>
			</div>
			</div>
			</div>
		)
	}

	const getMintSubscriptionBlock = () => {
		if ( pageMode === 'simple' ) { return null; }

		return (
			<div className="bw-subscib mb-6">
				<SubscriptionRenderer />
			</div>
		)
	}

	const getGeneralInfoBlock = () => {

		const showError = showErrors.find((item) => { return item === 'nftimg' });

		return (
			<div
				className="c-wrap"
				ref={ (e) => { scrollable.generalInfo = e } }
			>
				<div className="row">
					<div className="col-md-12">
						<div className="ai-generate select-coin text-green ml-2" style={{
							padding: "0.5em 2em 2em",
							textAlign: "center",
							alignItems: "center",
							display: "flex"
						}} onClick={() => { setWithAIModalOpened(true) }}><img src={icon_info} style={{
							maxWidth: "80px"
						}} />
							Want an AI-generated NFT?
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-md-5 mb-5 mb-md-0">
						{ getNftImgPreview() }
						{ getExternalUrlBlock() }
						<div>
							<div className="mb-2"> <small className="text-muted">File types supported: JPG, PNG, GIF, SVG, MP4, WEBM, MP3, WAV, OGG, GLB, GLTF. </small></div>
							<div> <small className="text-muted">Max size: 10 MB</small></div>
						</div>
					</div>
					<div className="col-md-7 pl-md-6">
						<div className="input-group">
							<label className="input-label">Name<sup className="text-red">*</sup></label>
							<input
								className={`nft-name input-control ${ showError ? 'has-error' : '' }`}
								type="text"
								value={ inputNftInfoName }
								placeholder="Name your NFT"
								onChange={(e) => {
									setInputNftInfoName(e.target.value);
									setShowErrors(showErrors.filter((item) => { return item !== 'emptyInfoName' }));
								}}
								onKeyPress={(e) => {
									if ( e.defaultPrevented) { return; }
								}}
							/>
						</div>
						<div className="input-group mb-6">
							<label className="input-label">Description<sup className="text-red">*</sup></label>
							<textarea
								className="nft-desc input-control"
								rows={ descriptionRows }
								value={ inputNftInfoDesc }
								placeholder="Enter a description"
								onChange={(e) => {
									setInputNftInfoDesc(e.target.value);
									setShowErrors(showErrors.filter((item) => { return item !== 'emptyInfoDesc' }));
								}}
								onKeyPress={(e) => {
									if ( e.defaultPrevented) { return; }
								}}
							></textarea>
						</div>
						{ getPropertiesBlock() }
						{ pageMode === 'simple' ? getErrorsBlock() : null }
						{ pageMode === 'simple' ? getCreateBtn() : null }
					</div>
				</div>
			</div>
		)
	}

	const getNftImageUrl = async (url: string) => {
		if(!url.toString().match(/http/i) || !url) {
			setNftImagePreview('');
		}
		else {
			const processResponse = (res:any) => {
				const statusCode = res.status
				const contentType = res.headers.get("Content-Type")
				const data = res.arrayBuffer()
				return Promise.all([statusCode, contentType, data]).then(res => ({
					statusCode: res[0],
					contentType: res[1],
					data: res[2]
				}))
			}
			await fetch( url )
				.then(processResponse)
				.then( res => {
					const { statusCode, contentType, data } = res
					if(statusCode === 200) {
						const blob = new Blob( [data] );
						const file = new FileReader();
						file.addEventListener('loadend', async (e) => {
							const result = e.target?.result;
							if (result) {
								setNftImagePreview(result);
								setNftImageMimeType(contentType);
								setInputNftImageUrl(url);
							}
						})
						file.readAsDataURL(blob);
					}
					else {
						console.log("Can not get an image from URL");
						setInputNftImageUrl('');
						setNftImagePreview('');
						setModal({
							type: _ModalTypes.error,
							title: `Cannot get image data from AI`,
							details: [
								`There was an error with getting result URL:`, 
								url,
								`Please save an image manually or try again later.`,
							]
						});			
					}
				})
				.catch( error => {
					console.error(error);
					console.log("Can not get an image from URL");
					setInputNftImageUrl('');
				});
		}
	}

	const getNftImgPreview = () => {

		if ( !nftImagePreview ) {
			return (
				<Dropzone
					onDrop={(files) => { previewOnDrop(files) }}
					noKeyboard={ true }
				>
					{/* accept={ "image/*,audio/mpeg,audio/ogg,audio/wav,audio/webm,video/webm,video/mp4,video/ogg,model/gltf-binary,model/gltf+json" } */}
					{
						({ getRootProps, getInputProps, isDragActive, open }) => (
							<div {...getRootProps({ className: `upload-container nft-upload mb-4 dropzone ${isDragActive ? 'file-dragged' : ''}` })}>
								<div className="upload-poopover">
									<div className="inner">
										<div className="h5 mb-0">
											<input {...getInputProps()} />
											{
												inputNftImageUrl ? (
													<span className="i-coin">
														<img src={ icon_loading } alt="" />
													</span>
												) : (
													<img src={ icon_i_drop } alt="" />
												)
											}
											<p>Drag and drop <br />your media file here <br />or click to browse</p>
										</div>
									</div>
								</div>
							</div>
						)
					}
				</Dropzone>
			)
		}

		return  (
			<div className="nft-upload-img">
				{ showNftImgPreview() }
				<button
					className="btn-del"
					onClick={() => {
						setNftImagePreview('');
						setInputNftImageUrl('');
					}}
				><img src={ icon_i_del} alt="" /></button>
			</div>
		)
	}

	const showNftImgPreview = () => {

		const nftImage = nftImagePreview ? nftImagePreview.toString() : ''
		if ( nftImageMimeType.includes("video/") ) {
			return (
				<video className="img" style={{ width: "100%", height: "100%" }} loop={ true } autoPlay={ true } muted={ true }>
					<source src={ nftImage } type={ nftImageMimeType } />
				</video>
			)
		}
		else if ( nftImageMimeType.includes("audio/") ) {
			return (
				<audio controls={ true } preload="none">
				    <source src={ nftImage } type={ nftImageMimeType } />
				</audio>
			)
		}
		else {
			return (
				<img
					src={ nftImage }
					className="img"
					alt=""
				/>
			)
		}
	}

	const previewOnDrop = (files: Array<File>) => {

		if(files[0]) {
			const imageMimeType = /(image|audio|video)\/(png|jpg|jpeg|svg|gif|webp|webm|mpeg|mp3|mp4|wav|ogg|glb|gltf)/i
			const contentType = files[0].type
			if (!contentType.match(imageMimeType)) {
				console.log("Unsupported MimeType file - " + contentType);
				return;
			}
			const file = new FileReader();
			file.addEventListener('loadend', async (e) => {
				const result = e.target?.result;
				if (result) {
					setNftImagePreview(result);
					setNftImageMimeType(contentType);
					setInputNftImageUrl('');
				}
			});
			file.readAsDataURL(files[0]);
		}
	}

	const getExternalUrlBlock = () => {

		if ( pageMode === 'simple' ) { return null; }

		const showError = showErrors.find((item) => { return item === 'emptyInfoUrl' });

		return (
			<div className="input-group">
				<label className="input-label">
					or enter an external URL
					<TippyWrapper
						msg="Paste full URL to the file from external source"
					></TippyWrapper>
					<span
						className="select-coin text-green ml-2"
						onClick={() => { setImageModalOpened(true) }}
						>Where do I get images?</span>
				</label>
				<input
					className={`input-control ${ showError ? 'has-error' : '' }`}
					type="text"
					value={ inputNftImageUrl }
					placeholder="https://"
					onChange={(e) => {
						getNftImageUrl(e.target.value);
						setInputNftImageUrl(e.target.value);
						setShowErrors(showErrors.filter((item) => { return item !== 'emptyInfoUrl' }));
					}}
					onKeyPress={(e) => {
						if ( e.defaultPrevented) { return; }
					}}
					disabled={ ((nftImagePreview && !inputNftImageUrl) ? true : false) }
				/>
			</div>
		)
	}

	const getPropertiesBlock = () => {

		if ( pageMode === 'simple' ) { return null; }

		if(!nftPropertiesDataTmp.length) {
			addNftEmptyProperty()
		}

		return (
			<div className="input-group mb-0">
				<button
					className="btn btn-outline"
					onClick={(e) => {
						setNftPropertiesDataTmp(nftPropertiesData);
						setNftPropertiesOpened(true);
					}}
				>
					{ nftPropertiesData.length ? (<>Edit</>) : (<>Add</>)} your NFT attributes
				</button>
				{ nftPropertiesData.length ? (
					<>
						<div className="input-label mt-6">NFT Attributes</div>
						<div className="c-wrap__table with-del-btn mt-3">
						{
							nftPropertiesData?.map((data, index)=>{
								const {type, name} = data
								return (
									<div
										key={index}
										className="item"
									>
										<div className="row">
											<div className="col-12 col-sm-5 mb-2">
												<span className="text-break text-muted">{type}</span>
											</div>
											<div className="col-12 col-sm-7 mb-2">
												<span className="text-break">{name}</span>
											</div>
											<button
												className="btn-del"
												onClick={() => { deleteNftProperties(index,false) }}
											><img src={ icon_i_del } alt="" /></button>
										</div>
									</div>
								)
							})
						}
						</div>
					</>
				) : null }
			</div>
		)
	}

	const showNftProperties = () => {

		return (
			<div className="modal">
				<div className="modal__inner" onClick={ (e) => {
						e.stopPropagation()
						if ((e.target as HTMLTextAreaElement).className === 'modal__inner') {
							setNftPropertiesOpened(false);
						}
					}}>
					<div className="modal__bg"></div>
					<div className="container">
						<div className="modal__content">
							<div
								className="modal__close"
								onClick={() => {
									setNftPropertiesOpened(false);
								}}
							>
								<svg width="37" height="37" viewBox="0 0 37 37" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path fillRule="evenodd" clipRule="evenodd" d="M35.9062 36.3802L0.69954 1.17351L1.25342 0.619629L36.4601 35.8263L35.9062 36.3802Z" fill="white"></path>
									<path fillRule="evenodd" clipRule="evenodd" d="M0.699257 36.3802L35.9059 1.17351L35.3521 0.619629L0.145379 35.8263L0.699257 36.3802Z" fill="white"></path>
								</svg>
							</div>
							<div className="modal__header">
								<div className="h2">NFT Attributes</div>
								<p>Traits describe additional information about NFT. They are stored on a decentralized storage and displayed at Opensea, Blur or other NFT marketplaces.</p>
								<div className="nft-property__form">
									<div className="row d-none d-sm-flex">
										<div className="col-sm-5">
											<div className="input-group">
												<label className="input-label">Trait Type
													<TippyWrapper
														msg="Add a name of the trait type that you want to save in NFT's metadata"
													></TippyWrapper>
												</label>
											</div>
										</div>
										<div className="col-sm-5">
											<div className="input-group">
												<label className="input-label">Value
													<TippyWrapper
														msg="Add a value of the trait that you want to save in NFT's metadata"
													></TippyWrapper>
												</label>
											</div>
										</div>
									</div>
									{
										nftPropertiesDataTmp?.map((data, index)=> {
											const {type, name} = data;
											return (
												<div
													key={index}
													className="row mb-4 mb-sm-0"
												>
													<div className="col-sm-5">
														<div className="input-group">
															<label className="input-label d-sm-none">Trait</label>
															<input
																type="text"
																className="input-control"
																value={type}
																placeholder="e.g. Level, Head, Body.."
																name="type"
																onChange={(e) => { changeNftProperties(index,e) }}
															/>
														</div>
													</div>
													<div className="col-sm-5">
														<div className="input-group">
															<label className="input-label d-sm-none">Value </label>
															<input
																type="text"
																className="input-control"
																value={name}
																name="name"
																onChange={(e) => { changeNftProperties(index,e) }}
															/>
														</div>
													</div>
													<div className="col-sm-2 col-del">
														<button
															className="btn-link px-0 btn-sm"
															onClick={() => { deleteNftProperties(index,true) }}
															> Delete</button>
													</div>
												</div>
											)
										})
									}
									<button
										className="btn btn-outline btn-sm w-auto"
										onClick={() => { addNftEmptyProperty() }}
									>Add a trait </button>
								</div>
								<div className="row mt-6">
									<div className="col-sm-5">
										<button
											className="btn w-100"
											onClick={() => {
												setNftPropertiesData(nftPropertiesDataTmp);
												setNftPropertiesOpened(false);
											}}
										>Save</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}

	const addNftEmptyProperty = () => {
		setNftPropertiesDataTmp([...nftPropertiesDataTmp, { type: '', name: '' }]);
	}

	const deleteNftProperties = (index: number, tmp: boolean) => {
		if (tmp) {
			const data = [...nftPropertiesDataTmp];
			data.splice(index, 1);
			setNftPropertiesDataTmp(data);
		}
		else {
			const data = [...nftPropertiesData];
			data.splice(index, 1);
			setNftPropertiesData(data);
		}
	}

	const changeNftProperties = (i: number, e: React.ChangeEvent<HTMLInputElement>) => {
		const {name, value} = e.target;
		const data = [...nftPropertiesDataTmp];
		data[i][name as keyof typeof data[0]] = value;
		setNftPropertiesDataTmp(data);
	}

	const getAdvancedOptionsBlock = () => {
		return true;
	}

	const getAdvancedOptionsBlockHidden = () => {
		return (
			<div className="c-wrap p-0">
				<div
					className={`c-wrap__toggle ${ advancedOptionsOpened ? 'active' : ''}`}
					onClick={() => {
						setAdvancedOptionsOpened(!advancedOptionsOpened)
					}}
				>
					<div><b>Advanced options</b></div>
				</div>
				<div className="c-wrap__dropdown">
					<div className="input-group mb-0">
						<label className="input-label pb-1">Metadata Hosting place:</label>
						<div className="mb-3">
							<label className="checkbox">
								<input
									type="radio"
									name="meta-hosting"
									value="2"
									checked={ metaHostingType === 2 }
									onChange={() => { setMetaHostingType(2) }}
								/>
								<span className="check"> </span>
								<span className="check-text">IPFS
									<TippyWrapper
										msg="Use IPFS decentralised data storage"
									></TippyWrapper>
								</span>
							</label>
						</div>
						<div className="mb-3">
							<label className="checkbox">
								<input
									type="radio"
									name="meta-hosting"
									value="1"
									checked={ metaHostingType === 1 }
									onChange={() => { setMetaHostingType(1) }}
								/>
								<span className="check"> </span>
								<span className="check-text">Swarm
									<TippyWrapper
										msg="Use Swarm decentralised data storage"
									></TippyWrapper>
								</span>
							</label>
						</div>
						<div className="mb-3">
							<label className="checkbox">
								<input type="radio" name="meta-hosting" disabled />
								<span className="check"> </span>
								<span className="check-text">Envelop
									<TippyWrapper
										msg="Work on support is in progress"
									></TippyWrapper>
								</span>
							</label>
						</div>
						<div className="mb-3">
							<label className="checkbox">
								<input type="radio" name="meta-hosting" disabled />
								<span className="check"> </span>
								<span className="check-text">Unstoppable NFT
									<TippyWrapper
										msg="Work on support is in progress"
									></TippyWrapper>
								</span>
							</label>
						</div>
					</div>
				</div>
			</div>
		)
	}

	const getCopiesBlock = () => {

		const showError = showErrors.find((item) => { return item === 'inputCopies' });

		if ( inputOutAssetType === _AssetType.ERC1155 ) {
			return (
				<div className="col-12 col-lg-3">
					<div className="input-group mb-md-0">
						<label className="input-label">Quantity
							<TippyWrapper
								msg="The amount of tokens for one minted NFT Token ID"
							></TippyWrapper>
						</label>
						<NumberInput
							value={ inputCopies }
							onChange={(e: number | undefined) => {
								setInputCopies(e || 2);
								setShowErrors( showErrors.filter((item) => { return item !== 'inputCopies' }) );
							}}
							min={ 2 }
							inputClass={ showError ? 'has-error' : '' }
						/>
					</div>
				</div>
			)
		}

	}

	const getBatchBlock = () => {

		const showError = showErrors.find((item) => { return item === 'inputBatch' });

		return (
			<div className="col-12 col-lg-3">
				<div className="input-group mb-md-0">
					<label className="input-label">Batch
						<TippyWrapper
							msg="The amount of NFT Token ID you want to mint per transaction (max 100)"
						></TippyWrapper>
					</label>
					<NumberInput
						value={ inputBatch }
						onChange={(e: number | undefined) => {
							const n = (e !== undefined && e > 100) ? 100 : e;
							setInputBatch(n || 1);
							setShowErrors( showErrors.filter((item) => { return item !== 'inputBatch' }) );
						}}
						min={ 1 }
						inputClass={ showError ? 'has-error' : '' }
					/>
				</div>
			</div>
		)
	}

	const getStandartSelectorBlock = () => {

		const prefix = (currentChain?.EIPPrefix) ? currentChain?.EIPPrefix : 'ERC';

		return (
			<div
				className="c-wrap"
				ref={ (e) => { scrollable.standart = e } }
			>
				<div className="row mb-3">
					<div className="col-12 col-lg-6">
						<div className="input-group mb-md-0">
							<label className="input-label">NFT Standard
								<TippyWrapper
									msg="Check type of NFTs that you want mint"
								></TippyWrapper>
							</label>
							<div className="row row-sm">
								<div className="col-auto my-2">
									<label className="checkbox">
										<input
											type="radio"
											name="nft-standard"
											value={ _AssetType.ERC721 }
											checked={ inputOutAssetType === _AssetType.ERC721 }
											onChange={() => { setInputOutAssetType(_AssetType.ERC721) }}
										/>
										<span className="check"> </span>
										<span className="check-text">{ (inputOutAssetType === _AssetType.ERC721) ? ( <b>{prefix}-721</b> ) : prefix + '-721' }</span>
									</label>
								</div>
								<div className="col-auto my-2">
									<label className="checkbox">
										<input
											type="radio"
											name="nft-standard"
											value={ _AssetType.ERC1155 }
											checked={ inputOutAssetType === _AssetType.ERC1155 }
											onChange={() => { setInputOutAssetType(_AssetType.ERC1155) }}
										/>
										<span className="check"> </span>
										<span className="check-text">{ (inputOutAssetType === _AssetType.ERC1155) ? ( <b>{prefix}-1155</b> ) : prefix + '-1155' }</span>
									</label>
								</div>
							</div>
						</div>
					</div>
					{ getCopiesBlock() }
					{ getBatchBlock() }
				</div>
				{ getMintFactoryBlock() }
			</div>
		)
	}

	const getAdvancedBlocks = () => {

		if ( pageMode === 'simple' ) { return null; }

		return (
			<div className="row">
				<div className="col-md-7 col-lg-7">
					{ getStandartSelectorBlock() }
				</div>
				<div className="col-md-5 col-lg-5">
					{ getAdvancedOptionsBlock() }
				</div>
			</div>
		)
	}

	const getButtonBlocks = () => {

		if ( pageMode === 'simple' ) { return null; }

		return (
			<>
				<div className="row">
					<div className="col-12 col-md-5 col-lg-5">
						{ getErrorsBlock() }
					</div>
				</div>
				<div className="row">
					<div className="col-12 col-md-7 col-lg-4">
						{ getCreateBtn() }
					</div>
				</div>
			</>
		)
	}
	
	const showFeedbackModal = () => {
		return (
			<div className="modal">
				<div className="modal__inner" onClick={ (e) => {
						e.stopPropagation()
						if ((e.target as HTMLTextAreaElement).className === 'modal__inner') {
							setFeedbackModalOpened(false)
						}
					}}>
					<div className="modal__bg"></div>
					<div className="container">
						<div className="modal__content">
							<div
								className="modal__close"
								onClick={() => {
									setFeedbackModalOpened(false)
								}}
							>
								<svg width="37" height="37" viewBox="0 0 37 37" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path fillRule="evenodd" clipRule="evenodd" d="M35.9062 36.3802L0.69954 1.17351L1.25342 0.619629L36.4601 35.8263L35.9062 36.3802Z" fill="white"></path>
									<path fillRule="evenodd" clipRule="evenodd" d="M0.699257 36.3802L35.9059 1.17351L35.3521 0.619629L0.145379 35.8263L0.699257 36.3802Z" fill="white"></path>
								</svg>
							</div>
							<div className="modal__header">
								<div className="h2 text-center">Your Feedback or Request</div>
								<ContactForm feedbackSent={ feedbackSent } setFeedbackSent={ setFeedbackSent } />
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}	

	const showModeModal = () => {
		return (
			<div className="modal">
				<div className="modal__inner" onClick={ (e) => {
						e.stopPropagation()
						if ((e.target as HTMLTextAreaElement).className === 'modal__inner') {
							setModeModalOpened(false)
						}
					}}>
					<div className="modal__bg"></div>
					<div className="container">
						<div className="modal__content">
							<div
								className="modal__close"
								onClick={() => {
									setModeModalOpened(false)
								}}
							>
								<svg width="37" height="37" viewBox="0 0 37 37" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path fillRule="evenodd" clipRule="evenodd" d="M35.9062 36.3802L0.69954 1.17351L1.25342 0.619629L36.4601 35.8263L35.9062 36.3802Z" fill="white"></path>
									<path fillRule="evenodd" clipRule="evenodd" d="M0.699257 36.3802L35.9059 1.17351L35.3521 0.619629L0.145379 35.8263L0.699257 36.3802Z" fill="white"></path>
								</svg>
							</div>
							<div className="modal__header">
								<div className="h2">What is Advanced Mode?</div>
								<ol>
									<li className="mb-4"><strong>NFT Attributes</strong> - with this ability you can create rich NFT metadata with unique traits and their values. <br /><em>No traits are used by default.</em></li>
									{/* <li className="mb-4"><strong>Metadata Hosting</strong> - you can choose where to host your NFT's metadata from two options currently: IPFS or Swarm, both are decentralized data storage networks.<br /><em>IPFS is used by default.</em></li> */}
									<li className="mb-4"><strong>ERC Standards</strong> - choose the right one standard for your NFT: either ERC-721 or ERC-1155, with the ability of setting the total supply quantity of NFTs. You can also even mint in a Batch!<br /><em>ERC-721 is used by default.</em></li>
									<li className="mb-4"><strong>Custom Contracts</strong> - deploy your own contracts via Envelop Mint Factory and be able to mint your NFTs on them!<br /><em>Our ERC-721 Mint Contract is used by default.</em></li>								</ol>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}

	const showImageModal = () => {
		return (
			<div className="modal">
				<div className="modal__inner" onClick={ (e) => {
						e.stopPropagation()
						if ((e.target as HTMLTextAreaElement).className === 'modal__inner') {
							setImageModalOpened(false)
						}
					}}>
					<div className="modal__bg"></div>
					<div className="container">
						<div className="modal__content">
							<div
								className="modal__close"
								onClick={() => {
									setImageModalOpened(false)
								}}
							>
								<svg width="37" height="37" viewBox="0 0 37 37" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path fillRule="evenodd" clipRule="evenodd" d="M35.9062 36.3802L0.69954 1.17351L1.25342 0.619629L36.4601 35.8263L35.9062 36.3802Z" fill="white"></path>
									<path fillRule="evenodd" clipRule="evenodd" d="M0.699257 36.3802L35.9059 1.17351L35.3521 0.619629L0.145379 35.8263L0.699257 36.3802Z" fill="white"></path>
								</svg>
							</div>
							<div className="modal__header">
								<div className="h2">Where do I get images?</div>
								<ol>
									<li className="mb-4"><strong>Unsplash</strong> is one of the most popular websites where you can find a vast collection of high-quality photos. There are many images related to cryptocurrency, such as Bitcoin and blockchain, available on Unsplash.</li>
									<li className="mb-4"><strong>Pixabay</strong> is another popular website where you can find free photos, vector images, and videos. There are also many images related to cryptocurrency available on Pixabay.</li>
									<li className="mb-4"><strong>Pexels</strong> is another website where you can find free images and videos. There are several images related to cryptocurrency available on Pexels.</li>
									<li className="mb-4"><strong>Freepik</strong> is a website where you can find free and premium images, vector images, and PSD files. There are several images related to cryptocurrency available on Freepik.</li>
									<li><strong>Flaticon</strong> is another website where you can find free and premium icons related to cryptocurrency. There are many icons related to Bitcoin, blockchain, and more available on Flaticon.</li>
								</ol>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}

	const showWithAIModal = () => {
		return (
			<div className="modal">
				<div className="modal__inner" onClick={ (e) => {
						e.stopPropagation()
						if ((e.target as HTMLTextAreaElement).className === 'modal__inner') {
							setWithAIModalOpened(false)
						}
					}}>
					<div className="modal__bg"></div>
					<div className="container">
						<div className="modal__content">
							<div className="c-info ">
								<div
									className="modal__close"
									onClick={() => {
										setWithAIModalOpened(false)
									}}
								>
									<svg width="37" height="37" viewBox="0 0 37 37" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path fillRule="evenodd" clipRule="evenodd" d="M35.9062 36.3802L0.69954 1.17351L1.25342 0.619629L36.4601 35.8263L35.9062 36.3802Z" fill="white"></path>
										<path fillRule="evenodd" clipRule="evenodd" d="M0.699257 36.3802L35.9059 1.17351L35.3521 0.619629L0.145379 35.8263L0.699257 36.3802Z" fill="white"></path>
									</svg>
								</div>
								<div className="c-info__img">
									<img src={icon_info} alt="" />
								</div>
								<div className="c-info__text">
									<div className="h2 mt-3">Let AI generate metadata</div>
									<p>Enter a <em>prompt</em> - an instruction for AI:</p>
									<div className="nft-property__form">
										<div className="row">
											<div className="col-12">
												<div className="input-group">
													<label className="input-label  d-sm-none">Prompt<em></em></label>
													<input
														type="text"
														className="input-control"
														value={withAIPrompt}
														placeholder="ultra realistic Envelop mascot"
														name="prompt"
														onChange={(e) => {
															setWithAIPrompt(e.target.value);
														}}
														onKeyPress={(e) => {
															if ( e.defaultPrevented) { return; }
														}}
														/>
												</div>
											</div>
											<div className="wf-settings__switcher">
												<div className="col-12 mb-3 ml-1">			
													<div className="mr-4 mt-3 mb-3">
														<label className="checkbox">
															<input
																type="checkbox"
																name="ai-image"
																checked={ withAIImage }
																onChange={(e) => {
																	setWithAIImage(e.target.checked); }}
															/>
															<span className="check"> </span>
															<span className="check-text">AI-generated Image
																<TippyWrapper
																	msg="Let AI generate an Image for your NFT"
																></TippyWrapper>
															</span>
														</label>
													</div>
													{ /*
														withAIImage ? (
															<div className="switcher">
																<input
																	className="toggle toggle-left"
																	type="radio"
																	id="model1-on"
																	checked={ withAIModel === 1 }
																	onChange={() => {
																		setWithAIModel(1);
																	}}
																/>
																<label
																	className="switcher__btn pl-4 pr-4"
																	style={{width:"auto"}}
																	htmlFor="model1-on"
																>StableDiffusion</label>
																<input
																	className="toggle toggle-right"
																	type="radio"
																	id="model2-on"
																	checked={ withAIModel === 2 }
																	onChange={() => {
																		setWithAIModel(2);
																	}}
																/>
																<label
																	className="switcher__btn pl-4 pr-4"
																	style={{width:"auto"}}
																	htmlFor="model2-on"
																>OpenAI DALL·E</label>
															</div>
														) : null */ }
												</div>
											</div>
											<div className="col-12 mt-1 mb-3 ml-1">
												<div className="d-inline-block input-group">
													<label className="checkbox">
														<input
															type="checkbox"
															name="ai-title"
															checked={ withAITitle }
															onChange={(e) => {
																setWithAITitle(e.target.checked); }}
														/>
														<span className="check"> </span>
														<span className="check-text">AI-generated Title and Description
															<TippyWrapper
																msg="Let AI generate a Title and Description text for your NFT"
															></TippyWrapper>
														</span>
													</label>
												</div>
											</div>
											<div className="col-6 col-sm-12" style={{
												textAlign: "center",
												alignItems: "center",
												display: "flex",
												margin: "auto"
											}}>
												<button
													className="btn w-100"
													disabled={ withAIPrompt === '' || sendingAIPrompt || (!withAIImage && !withAITitle) }
													onClick={() => {
														sendAiPrompt();
														setSendingAIPrompt(true);
														gaEventTracker('mint_ai_generate','AI-generated metadata');
													}}
												>{ 
													sendingAIPrompt ? ("Generating") : ("Generate")
												}</button>
												{
													sendingAIPrompt ? (
														<span className="i-coin ml-2">
															<img src={ icon_loading } alt="" />
														</span>
													) : null
												}
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}

	const sendAiPrompt = async () => {

		setInputNftImageUrl('');
		console.log("Sending a prompt:",withAIPrompt);
		try {
			// Send AI Data
			const AIData: any = await fetchAIData({
				address: userAddress ? userAddress : '',
				prompt: withAIPrompt,
				image: withAIImage ? ( withAIModel ? withAIModel : 2 ) : 0,
				title: withAITitle ? 1 : 0
			});
			console.log(AIData);
			if ( AIData !== undefined ) {
				if ('image' in AIData) {
					if ('result' in AIData['image']) {
						const aiImages = AIData['image']['result'];
						if (aiImages.length) {
							const aiImage = aiImages[0];
							if (aiImage.startsWith('http')) {
								getNftImageUrl(aiImage);
								setInputNftImageUrl(aiImage);
								console.log("AI image:",aiImage);
							}
							else {
								setNftImageMimeType("image/png");
								setNftImagePreview("data:image/png;base64," + aiImage);
							}
							setUsingAIImage(true);
						}
						else {
							console.log("AI image error:","no data");
							setInputNftImageUrl('');
							setNftImagePreview('');
						}
					}
					else if ('error' in AIData['image']) {
						const aiImageError = AIData['image']['error'];
						console.log("AI image error:",aiImageError);
						setInputNftImageUrl('');
						setNftImagePreview('');
					}
					else {
						console.log("AI image undefined error");
					}
				}
				if ('text' in AIData) {
					const aiText = AIData['text']['result'];
					const aiTitle = aiText.split('Description:')[0].replace("Title:",'').replace("|",'').trim();
					const aiDesc = (aiText.split('Description:')[1].replace("|",'').trim()).split('Title:')[0].trim();
					console.log("AI title:",aiTitle);
					console.log("AI desc:",aiDesc);
					setInputNftInfoName(aiTitle);
					setInputNftInfoDesc(aiDesc);
				}
				setSendingAIPrompt(false);
				setWithAIModalOpened(false);
			} else {
				console.log("Error with getting AI data");
				setSendingAIPrompt(false);
				setWithAIModalOpened(false);
				setModal({
					type: _ModalTypes.error,
					title: `Cannot get data from AI`,
					details: [
						`There was an error with getting data from AI.`,
						`Please try again later.`,
					]
				});
				return;
			}
		} catch(e: any) {
			setSendingAIPrompt(false);
			setWithAIModalOpened(false);
			setModal({
				type: _ModalTypes.error,
				title: `Cannot get data from AI`,
				details: [
					`There was an error with getting data from AI.`,
					`Please try again later.`,
					'',
					e.message || e,
				]
			});
			return;
		}

	}

	const savingMetadataModal = () => {
		return (
			<div className="modal">
				<div className="modal__inner" onClick={ (e) => {
					e.stopPropagation()
					if ((e.target as HTMLTextAreaElement).className === 'modal__inner') {
						setSavingMetadataOpened(false);
					}
				}}>
					<div className="modal__bg"></div>
					<div className="container">
						<div className="modal__content">
							<div
								className="modal__close"
								onClick={() => {
									setSavingMetadataOpened(false);
								}}
							>
								<svg width="37" height="37" viewBox="0 0 37 37" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path fillRule="evenodd" clipRule="evenodd" d="M35.9062 36.3802L0.69954 1.17351L1.25342 0.619629L36.4601 35.8263L35.9062 36.3802Z" fill="white"></path>
									<path fillRule="evenodd" clipRule="evenodd" d="M0.699257 36.3802L35.9059 1.17351L35.3521 0.619629L0.145379 35.8263L0.699257 36.3802Z" fill="white"></path>
								</svg>
							</div>
							<div className="modal__header">
								<div className="h2">{ (!deployingPopupOpened) ? "Minting your NFT" : "Deploying your Contract" }</div>
							</div>
							<div className="c-approve">
								{ (!deployingPopupOpened) ? savingMetadataQueue() : null }
								{ (!deployingPopupOpened) ? mintingStatusQueue() : null }
								{ (!deployingPopupOpened) ? mintingStatusResult() : null }
								{ (deployingPopupOpened) ? deployingContractQueue() : null }
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}

	const deployingContractQueue = () => {
		return(
			<div className={`c-approve__step ${ !mintFactoryContractCreated ? 'in-queue' : 'active' }`}>
				<div className="row">
					<div className="col-12 col-sm-auto order-2 order-sm-1"><span className="ml-2">Sending transaction<span className="dots">...</span></span> </div>
					<div className="col-12 col-sm-auto order-1 order-sm-1">
						<div className="status"><img className={ !mintFactoryContractCreated ? 'loader' : '' } src={ icon_loader } alt="" /></div>
					</div>
				</div>
			</div>
		)
	}

	const savingMetadataQueue = () => {
		let savingStatus = uploadedNftImageUrl ? 'in-queue' : 'active';
		if(uploadedNftImageUrl && uploadedNftJsonUrl) {
			savingStatus = "completed";
		}
		return(
			<div className={`c-approve__step ${ savingStatus }`}>
				<div className="row">
					<div className="col-12 col-sm-auto order-2 order-sm-1"><span className="ml-2">Saving your metadata<span className="dots">...</span></span> </div>
					<div className="col-12 col-sm-auto order-1 order-sm-1">
						<div className="status">{
							(savingStatus === "completed") ? (
								<b className="text-green">Completed</b>
							) : (
								<img className={ !uploadedNftImageUrl ? 'loader' : '' } src={ icon_loader } alt="" />
							)
						}</div>
					</div>
				</div>
			</div>
		)
	}

	const mintingStatusQueue = () => {
		if(uploadedNftImageUrl && uploadedNftJsonUrl) {
			return(
				<div className={`c-approve__step ${ mintedNftTxId ? 'in-queue' : 'active' }`}>
					<div className="row">
						<div className="col-12 col-sm-auto order-2 order-sm-1"><span className="ml-2">Minting NFT for you<span className="dots">...</span></span> </div>
						<div className="col-12 col-sm-auto order-1 order-sm-1">
							<div className="status"><img className={ !mintedNftTxId ? 'loader' : '' } src={ icon_loader } alt="" /></div>
						</div>
					</div>
				</div>
			)
		}
	}

	const mintingStatusResult = () => {
		if(uploadedNftJsonUrl && mintedNftTxId) {
			return(
				<div className="c-approve__step active">
					<div className="row">
						<div className="col-12 col-sm-auto order-2 order-sm-1"><span className="ml-2">Minted with transaction ID: <a href={ `${currentChain?.explorerBaseUrl}/tx/` + mintedNftTxId } target="_blank" rel="noreferrer">{ shortenHash(mintedNftTxId) }</a></span> </div>
						<div className="col-12 col-sm-auto order-1 order-sm-1">
							<div className="status"><img className={ !mintedNftTxId ? 'loader' : '' } src={ icon_loader } alt="" /></div>
						</div>
					</div>
				</div>
			)
		}
	}

	const shortenHash = (hash: any) => {
		return hash.substring(0, 4) + "..." + hash.substring(hash.length - 4, hash.length)
	}

	const hasErrors = () => {

		const errors: Array<{ msg: string, block: HTMLElement, showError?: string }> = [];

		if (!nftImagePreview) {
			if (!inputNftImageUrl) {
				errors.push({ msg: 'Choose a media file', block: scrollable.generalInfo })
			}
			else {
				errors.push({ msg: 'Wrong URL', block: scrollable.generalInfo })
			}
		}

		if (!inputNftInfoName) {
			errors.push({ msg: 'Add NFT name', block: scrollable.generalInfo })
		}

		if (!inputNftInfoDesc) {
			errors.push({ msg: 'Add NFT description', block: scrollable.generalInfo })
		}

		if (isNaN(+inputBatch) || +inputBatch < 1) {
			errors.push({ msg: 'Correct batch amount value', block: scrollable.standart })
		}

		if (isNaN(+inputCopies) || +inputCopies < 1) {
			errors.push({ msg: 'Correct supply amount value', block: scrollable.standart })
		}

		return errors;
	}

	const getErrorsBlock = () => {

		const errors = hasErrors();

		if ( !errors.length ) { return null; }

		return (
			<div className="c-errors mb-3">
				<div className="mb-3">Correct the following errors to enable Mint button:</div>
				<ul>
				{
					errors.map((item,key) => {
						return (
							<li key={ key }>
								<button
									className="btn-link"
									onClick={() => {
										if (item.block) {
											item.block.scrollIntoView();
										}
										if ( item.showError ) {
											setShowErrors([
												...showErrors.filter((iitem) => { return iitem !== item.showError }),
												item.showError
											]);
										}
									}}
								>
									{ item.msg }
								</button>
							</li>
						)
					})
				}
				</ul>
			</div>
		)
	}

	const getCreateBtn = () => {
		return (
			<button
				className="nft-mint-btn btn btn-lg w-100"
				disabled={ !!hasErrors().length }
				onClick={async () => {
					if ( !currentChain ) { return; }
					let _web3 = web3;
					let _userAddress = userAddress;
					let _currentChain = currentChain;

					if ( !web3 || !userAddress || await getChainId(web3) !== _currentChain.chainId ) {
						setLoading('Waiting for wallet');

						try {
							const web3Params = await getWeb3Force(_currentChain.chainId);

							_web3 = web3Params.web3;
							_userAddress = web3Params.userAddress;
							_currentChain = web3Params.chain;
							unsetModal();
						} catch(e: any) {
							setModal({
								type: _ModalTypes.error,
								title: 'Error with wallet',
								details: [
									e.message || e
								]
							});
						}
					}

					if ( !_web3 || !_userAddress || !_currentChain ) { return; }
					gaEventTracker('mint_create_submit','Hit Mint Submit');
					createSubmit(_currentChain, _web3, _userAddress);
				}}
			>Mint</button>
		)
	}

	const createSubmit = async (_currentChain: ChainType, _web3: Web3, _userAddress: string) => {

		setSavingMetadataOpened(true);
		// unset status and tx values if any previous stored
		setMintedNftTxId('');
		setUploadedNftImageUrl('');
		setUploadedNftJsonUrl('');
		let uploadedNftJsonHash = '';

		// process submit to Swarm
		if (metaHostingType === 1) {

			try {
				// Save data with Swarm
				const swarmData = await fetchSwarmStamp({
					name: inputNftInfoName,
					desc: inputNftInfoDesc,
					image: nftImagePreview.toString(),
					mime: nftImageMimeType,
					props: nftPropertiesData
				});

				if ( swarmData !== undefined ) {
					if ('image' in swarmData) {
						if (typeof swarmData['image'] === "string") {
							setUploadedNftImageUrl(swarmData['image']);
						}
					}
					if ('json' in swarmData) {
						if (typeof swarmData['json'] === "string") {
							const swarmHash = `bzz://${swarmData['json']}`;
							setUploadedNftJsonUrl(swarmHash);
							uploadedNftJsonHash = swarmHash;
						}
					}
				} else {
					console.log("Error with saving metadata to Swarm");
					setSavingMetadataOpened(false);
					setModal({
						type: _ModalTypes.error,
						title: `Cannot save metadata`,
						details: [
							`There was an error with saving metadata to Swarm.`,
							`Please try again or choose another metadata hosting option.`,
						]
					});
					return;
				}
			} catch(e: any) {
				setSavingMetadataOpened(false);
				setModal({
					type: _ModalTypes.error,
					title: `Cannot save metadata`,
					details: [
						`There was an error with saving metadata to Swarm.`,
						`Please try again or choose another metadata hosting option.`,
						'',
						e.message || e,
					]
				});
				return;
			}

		}
		// process submit to IPFS
		else if (metaHostingType === 2) {

			const formData = new FormData();
			const file = DataURIToBlob(nftImagePreview.toString());
			formData.append('file', file, inputNftInfoName);

			const metadata = JSON.stringify({
				name: inputNftInfoName + " image",
			});
			formData.append('pinataMetadata', metadata);

			const options = JSON.stringify({
				cidVersion: 0,
			});
			formData.append('pinataOptions', options);

			const url = "https://api.pinata.cloud/pinning/";
			const JWT = `Bearer ${metaPinataApi}`;

			try {
				// post image to IPFS
				const res = await axios.post(url + 'pinFileToIPFS', formData, {
					maxBodyLength: -1,
					headers: {
						'Content-Type': 'multipart/form-data',
						Authorization: JWT
					}
				});
				// console.log(res.data);

				if ('IpfsHash' in res.data) {
					const imageHash = `ipfs://${res.data.IpfsHash}`;
					setUploadedNftImageUrl(imageHash);

					let jsonData = JSON.stringify({
						"pinataOptions": {
							"cidVersion": 1
						},
						"pinataMetadata": {
							"name": inputNftInfoName + " JSON"
						},
						"pinataContent": {
							"name": inputNftInfoName,
							"description": inputNftInfoDesc,
							"image": (inputNftImageUrl && !usingAIImage) ? inputNftImageUrl : imageHash,
							"attributes": nftPropertiesData.map((item, index) => ({trait_type: item.type, value: item.name}))
						}
					});

					try {
						// post JSON to IPFS
						const resJson = await axios.post(url + 'pinJsonToIPFS', jsonData, {
							headers: {
								'Content-Type': 'application/json',
								Authorization: JWT
							}
						});
						console.log(resJson.data);

						if ('IpfsHash' in resJson.data) {
							const jsonHash = `ipfs://${resJson.data.IpfsHash}`;
							setUploadedNftJsonUrl(jsonHash);
							uploadedNftJsonHash = jsonHash;
						}

					} catch (e: any) {
						console.log(e);
						setModal({
							type: _ModalTypes.error,
							title: `Cannot save metadata`,
							details: [
								`There was an error with saving metadata to IPFS.`,
								`Please try again or choose another metadata hosting option.`,
								'',
								e.message || e,
							]
						});
						return;
					}

				}
				else {
					console.log("Error with IPFS image hash");
					return;
				}
			} catch (e: any) {
				console.log(e);
				setModal({
					type: _ModalTypes.error,
					title: `Cannot save metadata`,
					details: [
						`There was an error with saving metadata to Swarm.`,
						`Please try again or choose another metadata hosting option.`,
						'',
						e.message || e,
					]
				});
				return;
			}

		}

		// process with mint
		if (uploadedNftJsonHash) {

			// prepare form data
			const inputBatchValue = ( isNaN(+inputBatch) ) ? 1 : Number(inputBatch);
			const inputCopiesValue = ( isNaN(+inputCopies) ) ? 1 : Number(inputCopies);

			try {

				let nextTokenId = -1;
				let currentContract = '';
				let _implContract = '';
				let _isDefaultContract = true;

				if ( inputOutAssetType === _AssetType.ERC721 ) {
					if ( mintFactoryContract721Index !== 0 ) {
						_isDefaultContract = false;
						currentContract = mintFactoryContractList721[mintFactoryContract721Index].address;
						_implContract = implContract721;
					}
					else {
						currentContract = nftminterContract721;
						_implContract = nftminterContract721;
					}
					nextTokenId = await getTotalSupply721(_currentChain.chainId, currentContract, _implContract);
				}
				else if ( inputOutAssetType === _AssetType.ERC1155 ) {
					if ( mintFactoryContract1155Index !== 0 ) {
						_isDefaultContract = false;
						currentContract = mintFactoryContractList1155[mintFactoryContract1155Index].address;
						_implContract = implContract1155;
					}
					else {
						currentContract = nftminterContract1155;
						_implContract = nftminterContract1155;
					}
					nextTokenId = await getTotalSupply1155(_currentChain.chainId, currentContract, _implContract);
				}

				if(nextTokenId >= 0) {

					// mint that NFT
					const mintArgs = {
						tokenId: nextTokenId,
						tokenURI: uploadedNftJsonHash,
						batch: inputBatchValue,
						amount: inputCopiesValue,
						standart: inputOutAssetType,
						chainId: _currentChain.chainId,
						userAddress: _userAddress,
						nftminterContract: currentContract,
						implContract: _implContract,
						isDefaultContract: _isDefaultContract,
					};
					let txResp;
					try {
						console.log(mintArgs);
						txResp = await mintToken(
							_web3,
							mintArgs,
						);
					} catch(e: any) {
						setSavingMetadataOpened(false);
						console.log('Error:', e.message);
						setModal({
							type: _ModalTypes.error,
							title: `Cannot mint NFT`,
							details: [
								`Args: ${JSON.stringify(mintArgs)}`,
								'',
								e.message || e,
							]
						});
						return;
					}

					if ('transactionHash' in txResp) {
						console.log("Minted with txid " + txResp['transactionHash']);

						// get minted token IDs and contract address
						let viewNftTokenId = '';
						let viewNftContract = '';
						let viewNftObject = [];
						let tokenIds = [];

						viewNftObject = ( inputOutAssetType === _AssetType.ERC721 ) ? txResp.events.Transfer : txResp.events.TransferSingle;

						if( viewNftObject.length > 1 ) {
							viewNftObject.map( (item:any) => tokenIds.push( ( inputOutAssetType === _AssetType.ERC721 ) ? item.returnValues.tokenId : item.returnValues.id ) );
							viewNftContract = viewNftObject[0].address;
						}
						else {
							tokenIds.push( ( inputOutAssetType === _AssetType.ERC721 ) ? viewNftObject.returnValues.tokenId : viewNftObject.returnValues.id );
							viewNftContract = viewNftObject.address;
						}

						if ( tokenIds.length > 1 ) {
							await updateTotalSupply1155(_currentChain.chainId, tokenIds.length);
						}

						viewNftTokenId = tokenIds.map(i => i).join(',');
						setMintedNftContract(viewNftContract);
						setMintedNftTokenId(viewNftTokenId);
						setMintedNftTxId(txResp.transactionHash);
						setNftMintedPopupOpened(true);

						gaEventTracker('mint_nft_success','NFT Minted');

						unsetFormValues();

					}
					else {
						console.log('No TxID in response',txResp);
						setModal({
							type: _ModalTypes.error,
							title: `Cannot mint NFT`,
							details: [
								`Please try again later.`,
								'',
							]
						});
						return;
					}
				}
			} catch (e: any) {
				console.log(e);
				setModal({
					type: _ModalTypes.error,
					title: `Cannot mint NFT`,
					details: [
						`Please try again later.`,
						'',
						e.message || e,
					]
				});
				return;
			}
		}
	}

	const DataURIToBlob = (dataURI: string) => {
		const splitDataURI = dataURI.split(',');
		const byteString = splitDataURI[0].indexOf('base64') >= 0 ? atob(splitDataURI[1]) : decodeURI(splitDataURI[1]);
		const mimeString = splitDataURI[0].split(':')[1].split(';')[0];

		const ia = new Uint8Array(byteString.length);
		for (let i = 0; i < byteString.length; i++)
			ia[i] = byteString.charCodeAt(i);

		return new Blob([ia], { type: mimeString });
	}

	const unsetFormValues = () => {
		setSavingMetadataOpened(false);
		setInputNftInfoName('');
		setInputNftInfoDesc('');
		setInputNftImageUrl('');
		setWithAIPrompt('');
		setWithAIImage(true);
		setWithAITitle(false);
		setUsingAIImage(false);
		setFileReader([]);
		setNftImagePreview('');
		setNftImageMimeType('');
		setNftPropertiesData([]);
		setNftPropertiesDataTmp([]);
		setInputBatch(1);
		setInputCopies(2);
		setMetaHostingType(2);
		setUploadedNftImageUrl('');
		setUploadedNftJsonUrl('');
	}

	return (
		<CookiesProvider defaultSetOptions={{ path: '/mintnew' }}>
			<main className="s-main">
				<div className="container">
					<div className="wrap__header">
						<div className="h3 mt-0">Mint NFT</div>
						<div className="mt-10">
							<div className="row row-sm">
								<div className="col-12 my-2"><small>With this dApp you can mint ERC-721 or ERC-1155 NFT and make it smarter in few clicks in other dApps.</small></div>
								<div className="col-auto my-2"><small className="text-muted">* This is a BETA version. Reach us if you've found a bug or need an advanced functions - </small></div>
								<div className="nft-feedback col-auto my-2"><button className="btn btn-white btn-sm" onClick={() => { setFeedbackModalOpened(true); gaEventTracker('feedback_btn','Feedback Button'); }}>Feedback &amp; Feature Request</button></div>
							</div>
						</div>
					</div>
					<div className="wf-settings mb-4">
						<div className="row">
							{ getModeSelector() }
						</div>
					</div>
					{ isSubscriptionEnabled ? getMintSubscriptionBlock() : null }
					{ getGeneralInfoBlock() }
					{ getAdvancedBlocks() }
					{ getButtonBlocks() }
				</div>
			</main>
			{ nftMintedPopupOpened ? showNftMintedPopup() : null }
			{ mintFactoryPopupOpened ? showMintFactoryPopup() : null }
			{ feedbackModalOpened ? showFeedbackModal() : null }
			{ modeModalOpened ? showModeModal() : null }
			{ imageModalOpened ? showImageModal() : null }
			{ withAIModalOpened ? showWithAIModal() : null }
			{ nftPropertiesOpened ? showNftProperties() : null }
			{ savingMetadataOpened ? savingMetadataModal() : null }
		</CookiesProvider>
	)

}