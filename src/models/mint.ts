import {
	BigNumber,
	Web3,
	_AssetType,
	combineURLs,
	createContract,
	createAuthToken,
	decodeAssetTypeFromIndex,
	getDefaultWeb3,
	getABI,
	getUserAddress
} from "@envelop/envelop-client-core";

import { default as urljoin } from 'url-join';

export type APIOracleSign = {
	oracle_signature: string,
	oracle_signer: string,
	sender: string,
	source_chain: number,
	source_keeper: string,
	target_chain: number,
	target_contract: string,
	target_token_id: string,
	tx_hash: string,
}

export type SwarmStampBatchId = {
	name: string,
	desc: string,
}

export type AIData = {
	text: string,
	image: string,
	prompt: string
}

export const checkSubscriptionEnabled = async (chainId: number, contractAddress: string): Promise<boolean> => {

	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'collectionregistry', contractAddress);

	try {
		return await contract.methods.isEnabled().call();
	} catch(e: any) { throw e; }

}

export const getUserCollection = async (chainId: number, contractAddress: string, userAddress: string): Promise<number> => {

	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'collection', contractAddress);

	try {
		const collection = await contract.methods.getUsersCollections(userAddress).call();
		return collection.map((item: any) => { return { contractAddress: item.contractAddress, assetType: decodeAssetTypeFromIndex(item.assetType) } });

	} catch(e: any) { throw e; }

}

export const getTotalSupply721 = async (chainId: number, contractAddress: string, implAddress: string): Promise<number> => {
	
	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const abi = await getABI(chainId, implAddress);
	const contract = new web3.eth.Contract(abi as any, contractAddress);
	
	try {
		let output = parseInt(await contract.methods.totalSupply().call());
		return output;
	} catch(e: any) { throw e; }

}

export const getTotalSupply1155 = async (chainId: number, contractAddress: string, implAddress: string): Promise<number> => {

	const authToken = await createAuthToken();
	if ( authToken === '' ) {
		return -1;
	}

	let BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if ( !BASE_URL ) {
		console.log('No oracle base url in .env');
		return -1;
	}
	const url = combineURLs(BASE_URL, `/nftid/${chainId}`);

	let last;

	try {
		const resp = await fetch(url, {
			headers: {
				'Authorization': authToken,
			}
		});
		const res = await resp.json();
		if(res.hasOwnProperty("nextval")) {
			last = res.nextval;
		}
	} catch (e) {
		console.log('Cannot fetch NFT last ID', e);
	}

	if(typeof last === 'undefined') {
		const web3 = await getDefaultWeb3(chainId);
		if ( !web3 ) {
			throw new Error('Cannot connect to blockchain');
		}
		const abi = await getABI(chainId, implAddress);
		const contract = new web3.eth.Contract(abi as any, contractAddress);
		
		try {
			last = 0;
			while(true) {
				const supply = parseInt(await contract.methods.totalSupply(last).call());
				if(supply < 1) {
					break;
				}
				last++;
			}
		} catch(e: any) { throw e; }
	}
	return last;

}

export const updateTotalSupply1155 = async (chainId: number, batchSize: number): Promise<boolean> => {

	const authToken = await createAuthToken();
	if ( authToken === '' ) {
		return false;
	}

	let BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if ( !BASE_URL ) {
		console.log('No oracle base url in .env');
		return false;
	}
	const url = combineURLs(BASE_URL, `/nftid/${chainId}/update/${batchSize}`);

	try {
		const resp = await fetch(url, {
			headers: {
				'Authorization': authToken,
			}
		});
		const res = await resp.json();
		console.log(res);
		return true;
	} catch (e) {
		console.log('Cannot update NFT last ID', e);
		return false;
	}
	
}

export const getContractName = async (chainId: number, contractAddress: string, implAddress: string): Promise<string> => {
	
	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const abi = await getABI(chainId, implAddress);
	const contract = new web3.eth.Contract(abi as any, contractAddress);
	
	try {
		let output = await contract.methods.name().call();
		return output;
	} catch(e: any) { throw e; }

}

export const fetchSwarmStamp = async (params: {
	name: string,
	desc: string,
	image: string | ArrayBuffer,
	mime: string,
	props: Array<{
		type: string,
		name: string
	}>
}):Promise<Array<SwarmStampBatchId> | undefined> => {

	let BASE_URL = process.env.REACT_APP_ORACLE_API_MINT_URL;
	let url = BASE_URL || '';
	if ( !BASE_URL ) {
		BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
		if ( !BASE_URL ) { console.log('No oracle mint url in .env'); return undefined; }
		url = urljoin(BASE_URL, `mint/new/`);
	}
	// url  = urljoin(url, `new/`);
	console.log('url', url);

	let respParsed: Array<SwarmStampBatchId>;

	try {
		
		// set timeout for fetch request
		// const controller = new AbortController();
		// setTimeout(() => { controller.abort(); console.log("Fetch request 10s timeout occurred"); }, 10000);
		
		const requestOptions = {
	        method: 'POST',
	        // signal: controller.signal,
	        headers: {
	        	// 'Authorization': authToken,
	        	'Content-Type': 'application/json'
	        },
	        body: JSON.stringify({
	        	name: params.name,
	        	desc: params.desc,
	        	image: params.image,
	        	mime: params.mime,
	        	props: params.props
	        })
	    };
		const resp = await fetch(url, requestOptions);
		respParsed = await resp.json();
	} catch (e) {
		console.log('Cannot post newly minted NFT data', e);
		return undefined;
	}

	if ( !respParsed || 'error' in respParsed ) { return undefined; }

	return respParsed;
}

export const fetchAIData = async (params: {
	address: string,
	prompt: string,
	image: number,
	title: number
}):Promise<Array<AIData> | undefined> => {

	// const authToken = await createAuthToken();
	// if ( authToken === '' ) {
	// 	return undefined;
	// }

	let BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if ( !BASE_URL ) {
		console.log('No oracle base url in .env');
		return undefined;
	}
	const url = combineURLs(BASE_URL, `/mintai`);
	console.log('url', url);

	// let respParsed: Array<AIData>;
	let respParsed;

	try {
		
		const requestOptions = {
	        method: 'POST',
	        headers: {
	        	// 'Authorization': authToken,
	        	'Content-Type': 'application/json'
	        },
	        body: JSON.stringify({
	        	prompt: params.prompt,
	        	image: params.image,
	        	title: params.title
	        })
	    };
		const resp = await fetch(url, requestOptions);
		respParsed = await resp.json();
	} catch (e) {
		console.log('Cannot post AI data', e);
		return undefined;
	}

	// if ( !respParsed || 'error' in respParsed ) { return undefined; }

	return respParsed;
}

export const getOracleNftMinterSign = async (params: {
	address: string,
	chain_id: number,
	token_id: number,
	token_uri: string,
	batch: number,
	amount: number,
	standart: number
}):Promise<APIOracleSign | undefined> => {

	let BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if ( !BASE_URL ) { console.log('No oracle base url in .env'); return undefined; }

	const url  = urljoin(BASE_URL, '/web3/mintsign');

	let respParsed: APIOracleSign;

	try {
	    const requestOptions = {
	        method: 'POST',
	        headers: {
	        	// 'Authorization': authToken,
	        	'Content-Type': 'application/json'
	        },
	        body: JSON.stringify({
	        	address: params.address,
	        	chainId: params.chain_id,
	        	tokenId: params.token_id.toString(),
	        	tokenURI: params.token_uri,
	        	batch: params.batch,
	        	amount: params.amount,
	        	standart: params.standart
	        })
	    };
		const resp = await fetch(url, requestOptions);
		respParsed = await resp.json();
	} catch (e) {
		console.log('Cannot fetch oracle signature', e);
		return undefined;
	}

	if ( !respParsed || 'error' in respParsed ) { return undefined; }

	return respParsed;
}

export const mintToken = async (
	web3: Web3,
	params: {
		tokenId: number,
		tokenURI: string,
		batch: number,
		amount: number,
		standart: number,
		chainId: number,
		userAddress: string,
		nftminterContract: string,
		implContract: string,
		isDefaultContract: boolean,
	}
) => {
	
	const abi = await getABI(params.chainId, params.implContract);
	const contract = new web3.eth.Contract(abi as any, params.nftminterContract);

	let tx;	
	
	if ( params.batch > 1 ) {
		// Prepare batch
		let addresses = [];
		let tokenIds = [];
		let tokenURIs = [];
		let amounts = [];
		for ( let i = 0; i < params.batch; i++ ) {
			addresses.push(params.userAddress as any);
			tokenIds.push(params.tokenId++ as any);
			tokenURIs.push(params.tokenURI as any);
			amounts.push(params.amount as any);
		}
		if (params.isDefaultContract) {
			tx = ( params.standart === _AssetType.ERC721 ) ? contract.methods.mintWithURIBatch(addresses, tokenURIs) : contract.methods.mintWithURIBatch(addresses, tokenIds, amounts, tokenURIs);
		}
		else {
			tx = ( params.standart === _AssetType.ERC721 ) ? contract.methods.mintWithURIBatch(addresses, tokenIds, tokenURIs) : contract.methods.mintWithURIBatch(addresses, tokenIds, amounts, tokenURIs);
		}
	}
	else {
		if (params.isDefaultContract) {
			tx = ( params.standart === _AssetType.ERC721 ) ? contract.methods.mintWithURI(params.userAddress, params.tokenURI) : contract.methods.mintWithURI(params.userAddress, params.tokenId, params.amount, params.tokenURI);
		}
		else {
			tx = ( params.standart === _AssetType.ERC721 ) ? contract.methods.mintWithURI(params.userAddress, params.tokenId, params.tokenURI) : contract.methods.mintWithURI(params.userAddress, params.tokenId, params.amount, params.tokenURI);
		}
	}

	// slightly adjust gas for Goerli and Polygon
	// if([5,137].includes(params.chainId as number)) {
	// 	estimatedGas = new BigNumber(estimatedGas).plus(new BigNumber(100000)).toString();
	// }

	return tx.send({ from: params.userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null });
}

export const buySubscription = async (
	web3: Web3,
	subscriptionsContract: string,
	registryContract: string,
	params: {
		chainId: number,
	}
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'mintsubscription', subscriptionsContract);

	const tx = contract.methods.buySubscription(
		registryContract,
		0,
		0,
		userAddress,
		userAddress,
	);
	
	// slightly adjust gas for Goerli and Polygon
	// if([5,137].includes(params.chainId as number)) {
	// 	estimatedGas = new BigNumber(estimatedGas).plus(new BigNumber(100000)).toString();
	// }

	return tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null });
}

export const deployNewCollection = async (
	web3: Web3,
	implContractAddress: string,
	registryContract: string,
	params: {
		chainId: number,
		_name: string,
		_symbol: string,
		_baseurl: string,
	}
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	let BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	let url;
	if ( !BASE_URL ) { console.log('No oracle url in .env'); return undefined; }
	url  = urljoin(BASE_URL, `metadata/`);
	console.log('url', url);

	const contract = await createContract(web3, 'mintregistry', registryContract);

	const tx = contract.methods.deployNewCollection(
		implContractAddress,
		userAddress,
		params._name,
		params._symbol,
		url,
	);

	// slightly adjust gas for Goerli and Polygon
	// if([5,137].includes(params.chainId as number)) {
	// 	estimatedGas = new BigNumber(estimatedGas).plus(new BigNumber(100000)).toString();
	// }

	return tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null });
}
